#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/i2c.h>

void i2c_setup(void) {
  rcc_periph_clock_enable(RCC_I2C1);
  rcc_periph_clock_enable(RCC_AFIO);
  
  gpio_primary_remap(0, AFIO_MAPR_I2C1_REMAP);
  gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ,
		GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN,
		GPIO_I2C1_RE_SCL | GPIO_I2C1_RE_SDA);

  i2c_peripheral_disable(I2C1);
  // APB1 clock speed. This should really be handled with something in
  // libopencm3, but ¯\_(ツ)_/¯
  i2c_set_clock_frequency(I2C_CR2_FREQ_48MHZ);
  
}
