#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/spi.h>

#include "leds.h"

static rgbcolor_t led_colors[LED_COUNT] = {0};

void led_setup(void) {
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ,
		GPIO_CNF_OUTPUT_ALTFN_PUSHPULL,
		GPIO5|GPIO7);

  spi_reset(SPI1);
  
  
  // clock and data pins are inverted
  spi_init_master(SPI1,
		  SPI_CR1_BAUDRATE_FPCLK_DIV_64,
		  SPI_CR1_CPOL_CLK_TO_0_WHEN_IDLE, // the chip expects 1, but it's inverted
		  SPI_CR1_CPHA_CLK_TRANSITION_2,
		  SPI_CR1_DFF_8BIT,
		  SPI_CR1_MSBFIRST);
  spi_enable_software_slave_management(SPI1);
  spi_set_nss_high(SPI1);

  spi_enable(SPI1);
}

void led_setcolor(int ledno, rgbcolor_t color) {
  led_colors[ledno] = color;
}

static void write_frame(uint32_t value) {
  for (int i = 0; i < 4; i++) {
    // The inversion here is because the data line is inverted for level shiftin
    spi_send(SPI1, ~(value >> (3-i)) & 0xff);
  }
}

void led_commit(void) {
  // TODO: This should be interrupt-driven
  write_frame(0x00);
  for (int i = 0; i < LED_COUNT; i++) {
    write_frame(0xFF000000u | led_colors[i]);
  }
  write_frame(~0x00);
  // This second one is in case the LEDs are actually APA102's, which
  // delays the data one cycle for each bit
  write_frame(~0x00);
}

