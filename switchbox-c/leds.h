#pragma once
#include <stdint.h>

#define LED_COUNT 8
typedef uint32_t rgbcolor_t;

static inline rgbcolor_t rgb(uint8_t r, uint8_t g, uint8_t b) {
  uint32_t result = 0;
  result = (result << 8) | r;
  result = (result << 8) | g;
  result = (result << 8) | b;
  return result;
}

void led_setup(void);
void led_setcolor(int ledno, rgbcolor_t color);
void led_commit(void);
