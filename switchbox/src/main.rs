#![no_std]
#![no_main]
#![feature(const_fn)]
#![feature(use_extern_macros)]
#![feature(proc_macro_gen)]
#![allow(non_camel_case_types, dead_code)]

extern crate cortex_m;
#[macro_use]
extern crate cortex_m_rt;
extern crate stm32f1;
extern crate cortex_m_semihosting;
extern crate panic_semihosting;
extern crate cortex_m_rtfm as rtfm;

use cortex_m_rt::ExceptionFrame;
use rtfm::app;

mod can;

app!{
    device: stm32f1::stm32f103,
    resources: {
        static CAN: ::can::Device;
    },
    tasks: {
        can_rx: {
            path: can::recv,
            resources: [CAN],
            interrupt: USB_LP_CAN_RX0,
        },
        can_tx: {
            path: can::xmit,
            resources: [CAN],
            interrupt: USB_HP_CAN_TX,
        },
    },
    idle: {},
}

fn init(mut ctx: init::Context) -> init::LateResources {
    init::LateResources{
        CAN: can::Device::new(
            &mut ctx.device.RCC,
            ctx.device.CAN1,
            0x101,
        )
    }
}

#[inline(always)]
fn idle(_ctx: idle::Context) -> ! {
    loop {
        cortex_m::asm::wfi();
    }
}

exception!(HardFault, hard_fault);

#[inline(always)]
fn hard_fault(ef: &ExceptionFrame) -> ! {
    panic!("HardFault at {:#?}", ef);
}

exception!(*, default_handler);

#[inline(always)]
fn default_handler(irqn: i16) {
    panic!("Unhandled exception (IRQn = {})", irqn);
}
