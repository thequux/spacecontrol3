use stm32f1::stm32f103::{self, CAN1};
pub struct Device {
    dev: CAN1,
    id: u16,
}

#[derive(Copy,Clone,PartialEq,Eq,Default)]
struct Address(u32);

// Filter usage:
//  0: 
impl Address {
    pub const fn normal(id: u16) -> Address {
        Address((id as u32) << 21)
    }

    pub const fn extended(id: u32) -> Address {
        Address(id << 3 | 0x4)
    }

    pub const fn devcall(dev_id: u16, cmd: u32) -> Address {
        Self::extended(0x70 << 18 | (dev_id as u32) | cmd << 12 )
    }

    pub const fn req(self) -> Self {
        Address(self.0 | 0x2)
    }

    pub const fn from_bxcan(reg: u32) -> Address {
        Address(reg & !1u32)
    }

    pub const fn is_extended(self) -> bool {
        self.0 & 0x4 != 0
    }

    pub const fn is_request(self) -> bool {
        self.0 & 0x2 != 0
    }

    pub const fn get_normal(self) -> u16 {
        (self.0 >> 21) as u16
    }
    
    pub const fn get_extended(self) -> u32 {
        self.0 >> 3
    }

    pub const fn get_bxcan_addr(self) -> u32 {
        self.0
    }
}

#[derive(Copy,Clone,PartialEq,Eq,Default)]
pub struct Msg {
    address: Address,
    length: u8,
    filter_id: u8,
    timestamp: u16,
    msg: [u8; 8],
}

impl Device {
    pub fn new(rcc: &mut stm32f103::RCC, dev: CAN1, dev_id: u16) -> Self {
        // Configure device
        // Filter usage:
        // 0: Ping (0x70r)
        // 1: LED set
        // 2: Device info

        unsafe {
            dev.f0r1.write(|r| r.bits(Address::normal(0x70).req().get_bxcan_addr()));
            dev.f0r2.write(|r| r.bits(Address::devcall(dev_id, 0x20).get_bxcan_addr()));
            dev.f1r1.write(|r| r.bits(Address::devcall(dev_id, 0x00).req().get_bxcan_addr()));
        }

        Device {
            dev,
            id: dev_id,
        }
    }
}

pub fn xmit(ctx: ::can_tx::Context) {
}

impl Device {
    pub fn fetch_pkt(&mut self) -> Option<Msg> {
        // Make sure that any acking is done and that there is a waiting packet
        loop {
            let rf0r = self.dev.can_rf0r.read();
            if rf0r.rfom0().bit() {
                // Partially-acked packet
                continue;
            }
            if rf0r.fmp0().bits() == 0 {
                // no waiting packets
                return None;
            } else {
                // process packet
                break;
            }
        }

        // read all necessary registers
        let rdt = self.dev.can_rdt0r.read();
        let ri = self.dev.can_ri0r.read();
        let msg_l = self.dev.can_rdl0r.read();
        let msg_h = self.dev.can_rdh0r.read();
        
        // ack the packet
        self.dev.can_rf0r.modify(|_,w| w.rfom0().set_bit());
        
        return Some(Msg {
            msg: [
                msg_l.data0().bits(),
                msg_l.data1().bits(),
                msg_l.data2().bits(),
                msg_l.data3().bits(),
                
                msg_h.data4().bits(),
                msg_h.data5().bits(),
                msg_h.data6().bits(),
                msg_h.data7().bits(),
            ],
            length: rdt.dlc().bits(),
            filter_id: rdt.fmi().bits(),
            timestamp: rdt.time().bits(),
            address: Address::from_bxcan(ri.bits()),
        })
    }

    
}
pub fn recv(ctx: ::can_rx::Context) {
    while let Some(msg) = ctx.resources.CAN.fetch_pkt() {
        
    }
}
