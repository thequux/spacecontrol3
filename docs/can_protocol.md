General considerations
======================

Each device has dynamically assigned a 12-bit "bus" ID and at up to 96
bits of "fixed" ID. Activity alerts all include the bus ID in the
packet.

All values are sent little-endian.

Messages
========


